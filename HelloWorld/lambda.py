# Using of lambda expressions for every element in the list
print(list(map(lambda x: (x ** 2) / 2, [7, 5, 2])))
