def order(*args, **kwargs):
    print("Hello! I would like {} {}".format(args[0], kwargs['food']))


#order(1, 2, food='sandwich', drink='coffee')


def is_cat_here(*args):
    return 'cat' in args


print(is_cat_here(4, 5, 'cat'))


def is_item_here(item, *args):
    return item in args


print(is_item_here(4, 4, 5, 'dog'))


def your_favorite_color(my_color, **kwargs):
    if 'color' in kwargs:
        print("My favorite color is {}, but {} is also pretty good".format(my_color, kwargs['color']))
    else:
        print('My favorite color is {}. What is your favorite color?')


your_favorite_color('red', color='green')